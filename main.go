package main

import (
	"fmt"
	"os"
)

func main() {
	for {
		fmt.Println("\nChoose your function: ")
		fmt.Println("\t1. Fibonacci recursive")
		fmt.Println("\t2. Fibonacci dynamic programming")
		fmt.Println("\t0. Exit")
		fmt.Print("\n: ")

		var choice int
		fmt.Scanln(&choice)

		switch choice {
		case 1:
			runFib(true)
		case 2:
			runFib(false)
		case 0:
			fmt.Println("Exiting...")
			os.Exit(0)1
		default:
			fmt.Println("Invalid choice.  Exiting")
		}
	}
}

func runFib(recursive bool) {
	var num int = 0
	fmt.Print("How many Fibonacci numbers?: ")
	fmt.Scanln(&num)
	if recursive {
		var result = fibr(num)
	}
	else {
		var result = fibd(num)
	}
	fmt.Println("Result: ", result)
}

func fibr(n int) int {
	if n == 0 {
		return 0
	} else if n == 1 {
		return 1
	} else {
		return fib(n - 1) + fib(n - 2)
	}
}

func fibd(n int) int {
	var F[0, 1]int		// Array for dynamic results
	for i = 1 to n {
		F[i] = F[i -1] + F[i - 2]
	}

	return F[n]
}
